#! /bin/bash

USAGE="\n\nUsage: sbatch syntax $0 fastqfile\n\n
Where\n
sbatch syntax is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p core -n 1 -t 24:00:00 -J jobname
-e xxx.err -o xxx.out\n\n"

module load bioinfo-tools
module load FastQC/0.11.2

echo "Starting FastQC analysis of file "$1

fastqc $1


