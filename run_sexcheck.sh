#! /bin/bash

USAGE="\n

Usage: <sbatch syntax> $0 normal.vcf \n\n

Where\n 
<sbatch syntax> is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p node -n 16  -t 24:00:00 -J bam2vcf
-e somename.err -o somename.out\n
and\n
configfile.sh contains paths to reference files and project info etc.  \n\n"

if test -z $3; then
	echo -e $USAGE
	exit
fi

#Input data
NORMAL_VCF=$1

#Modules
module load bioinfo-tools
module load plinkseq


#Reading config file
. $CONFIGFILE

echo "normal bam file: "$NORMAL
echo "tumor bam file: "$TUMOR

prefix=${NORMAL%".vcf"}

## PLINKSEQ: Simple sample statistics based on variants only (minutes-hours).
outfile=$prefix".istats"
outfileX=$prefix"_chrX.istats"

### Calculate chr-X heterozygosity to find sex discrepancy
##  May need to change cutoffs for normal females and males in R-script after viewing distribution of het-values
echo "Running chrX heterozygosity: Results will be written to $outfileX"
pseq $vcffile i-stats --mask reg=chrX > $outfileX


