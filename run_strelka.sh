#! /bin/bash

USAGE="\n

Usage: <sbatch syntax> $0 normal.bam tumor.bam configfile.sh \n\n

Where\n
<sbatch syntax> is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p node -n 16  -t 24:00:00 -J jobname
-e somename.err -o somename.out\n
and\n
configfile.sh contains paths to reference files and project info etc.  \n\n"

if test -z $3; then
echo -e $USAGE
exit
fi

set -euo pipefail

#Input data
NORMAL=$1
TUMOR=$2
CONFIGFILE=$3

#Path to strelka install dir
STRELKA_INSTALL_DIR=/home/malin/bin

#Path to work dir
WORK_DIR=$PWD

#Reading config file
. $CONFIGFILE

echo "normal bam file: "$NORMAL
echo "tumor bam file: "$TUMOR

normalprefix=${NORMAL%".md.real.recal.bam"}
tumorprefix=${TUMOR%".md.real.recal.bam"}

SPAIR=$normalprefix"_"$tumorprefix
echo "Sample pair "$SPAIR


# Step 2. Copy configuration ini file from default template set to a
#         local copy, possibly edit settings in local copy of file:
#
cp $STRELKA_INSTALL_DIR/etc/strelka_config_bwa_default.ini strelka_config.ini

# Step 3. Configure:
#
$STRELKA_INSTALL_DIR/bin/configureStrelkaWorkflow.pl \
--normal=$NORMAL \
--tumor=$TUMOR \
--ref=$REFERENCE \
--config=strelka_config.ini --output-dir=$WORK_DIR/strelka

# Step 4. Run Analysis
#         This example is run using 8 cores on the local host:
#
cd $WORK_DIR/strelka
make -j 16
if test $? = 0;then
    echo "Strelka went well!"
fi

