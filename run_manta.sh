#! /bin/bash

USAGE="\n

Usage: <sbatch syntax> $0 normal.bam tumor.bam configfile.sh \n\n

Where\n 
<sbatch syntax> is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p node -n 16  -t 24:00:00 -J bam2vcf
-e somename.err -o somename.out\n
and\n
configfile.sh contains paths to reference files and project info etc.  \n\n"

if test -z $3; then
	echo -e $USAGE
	exit
fi

#Input data
NORMAL=$1
TUMOR=$2
CONFIGFILE=$3


#Modules
module load bioinfo-tools
module load manta
module load samtools


#Reading config file
. $CONFIGFILE

echo "normal bam file: "$NORMAL
echo "tumor bam file: "$TUMOR

normalprefix=${NORMAL%".md.real.recal.bam"}
tumorprefix=${TUMOR%".md.real.recal.bam"}

SPAIR=$normalprefix"_"$tumorprefix
echo "Sample pair "$SPAIR

if [ ! -f $NORMAL.bai ]
then
    echo "indexing $NORMAL"
    samtools index $NORMAL
fi

if [ ! -f $TUMOR.bai ]
then
echo "indexing $TUMOR"
samtools index $TUMOR
fi

configManta.py \
--normalBam $NORMAL \
--tumorBam $TUMOR \
--referenceFasta $REFERENCE \
--runDir manta

cd manta
./runWorkflow.py -m local -j 16
