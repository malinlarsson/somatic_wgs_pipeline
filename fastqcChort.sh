#! /bin/bash


USAGE="\nfastqc analysis of all fastq files in cohort. \n\n

Usage: $0 <datadir> <fastqlist> \n\n

Where\n
<datadir> is the root folder for the fastq files\n
<fastqlist> contain the relative paths (from root folder) to fastq files.\n\n"



if test -z $2; then
	echo -e $USAGE
	exit
fi

datadir=$1
fastqfiles=$2

module load bioinfo-tools
module load FastQC

echo $fastqfile

if test ${datadir:(-1)} == '/';
then
echo "Must remove /"
datadir=${datadir%?}
fi


#Analyze all samples in cohort
while read fastqfile
do
    thisfile=$datadir"/"$fastqfile
    echo "Submitting: "$thisfile
    sbatch /proj/b2011185/nobackup/wabi/scripts/run_fastqc.sh $thisfile


done < $fastqfiles
