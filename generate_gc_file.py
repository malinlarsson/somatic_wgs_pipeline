#! /usr/bin/env python

import sys, re, math, random, os

if len(sys.argv)<2:
    print "Usage: %s locifile referece_file\n" %sys.argv[0]
    sys.exit(0)

locifile = sys.argv[1]
reference = sys.argv[2]
outfile = "1000G_maf0.3.gc"


print "Reference genome file: "+reference

loci={}
for l in open(locifile, 'r'):
    line=l.strip()
    info=line.split("\t")
    loci[info[0]+"_"+info[1]]=[info[0],info[1]]
print loci['1_14930'][0]


#win=[12, 25, 50, 100, 250, 500, 1000, 2500, 5000, 10000, 25000, 50000, 100000, 250000, 500000, 1000000, 2500000, 5000000]

win=[12, 25, 50, 100, 250, 500]

#Generate gc data for all window sizes
for w in win:
    print w
    bedfile=str(w)+".bed"
    gcfile=str(w)+".gc"
    wf = open(bedfile, 'w')
    for l in loci:
        wf.write("%s\t%s\t%s\t%s\n" %(loci[l][0],(int(loci[l][1])-w),(int(loci[l][1])+w),l))
    cmdstring="bedtools nuc -fi "+reference+" -bed "+bedfile+" | awk '{print $4\"\\t\"$6}' > "+gcfile
    os.system(cmdstring)

#Collect the gc data into one file

gc={}
for w in win:
    gc[w]={}
    gcfile=str(w)+".gc"
    print gcfile
    for gcl in open (gcfile, 'r'):
        l=gcl.strip()
        data=l.split("\t")
        gc[w][data[0]]=data[1]
        
of = open (outfile, 'w')
#of.write("Chr\tPosition\t25bp\t50bp\t100bp\t200bp\t500bp\t1000bp\t2000bp\t5000bp\t10000bp\t20000bp\t50000bp\t100000bp\t200000bp\t500000bp\t1M\t2M\t5M\t10M\n")
of.write("ID\tChr\tPosition\t25bp\t50bp\t100bp\t200bp\t500bp\t1000bp\n")
for l in loci:
    of.write ("%s_%s\t%s\t%s\t" %(loci[l][0], loci[l][1], loci[l][0], loci[l][1]))
    for w in win:
        if l in gc[w]:
            of.write("%s\t" %gc[w][l])
        else:
            of.write("NA\t")
    of.write("\n")
        
        

