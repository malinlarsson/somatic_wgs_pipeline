#! /bin/bash

USAGE="\n

Usage: <sbatch syntax> $0 BAM CONFIGFILE\n\n

Where\n
<sbatch syntax> is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p node -n 16  -t 24:00:00 -J bam2vcf
-e somename.err -o somename.out\n
BAM = bam file \n
CONFIGFILE = configfile containing GATK reference\n\n"

if test -z $1; then
echo -e $USAGE
exit
fi

#Modules
module load bioinfo-tools
module load GATK
module load R/3.2.2
set -euo pipefail

#Input
BAM=$1
CONFIGFILE=$2

. $CONFIGFILE

prefix=${BAM%".bam"}

if [ ! -f "$BAM" ]; then
echo "Bamfile $BAM not found!"
exit 1
fi
if [ ! -f "$CONFIGFILE" ]; then
echo "Configfile $CONFIGFILE not found!"
exit 1
fi

###############################################
#
# Recalibrating (gatk)
#
###############################################


echo "#######  Recalibrating with GATK"
echo "Infile = "$BAM
echo "Recal table = "$prefix".table"
echo "Outfile = "$prefix".recal.bam"

#Create the before-recalibration table
java -jar $GATK_HOME/GenomeAnalysisTK.jar \
-T BaseRecalibrator \
-R $REFERENCE \
-I $BAM \
-knownSites $MILLS \
-knownSites $KGINDELS \
-knownSites $DBSNP \
-o $prefix".table" \
-nct 16 \
-rf BadCigar

if test $? = 0;then
echo "BaseRecalibrator went well!"
fi
wait

#print the reads
java -jar $GATK_HOME/GenomeAnalysisTK.jar \
-T PrintReads \
-R $REFERENCE \
-I $BAM \
-BQSR $prefix".table" \
-o $prefix".recal.bam" \
-nct 16 \
-rf BadCigar
if test $? = 0;then
echo "PrintReads went well!"
fi
wait

#create the after-recalibration table
java -jar $GATK_HOME/GenomeAnalysisTK.jar \
-T BaseRecalibrator \
-R $REFERENCE \
-I $BAM \
-knownSites $MILLS \
-knownSites $KGINDELS \
-knownSites $DBSNP \
-BQSR $prefix".table" \
-o $prefix".recal.table" \
-nct 16 \
-rf BadCigar
if test $? = 0;then
    echo "BaseRecalibrator went well!"
    rm $BAM
    if [ -f $prefix".bai" ]; then
        rm $prefix".bai"
    elif [ -f $prefix".bam.bai" ]; then
        rm $prefix".bam.bai"
    fi
fi
wait

#create before and after plots
java -jar $GATK_HOME/GenomeAnalysisTK.jar \
-T AnalyzeCovariates \
-R $REFERENCE \
-before $prefix".table" \
-after $prefix".recal.table" \
-plots $prefix".recal_plots.pdf"
if test $? = 0;then
echo "Plot recalibration tables  went well!"
fi
wait
