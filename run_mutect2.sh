#! /bin/bash

USAGE="\n

Usage: $0 normal.bam tumor.bam configfile.sh \n\n

Where\n
configfile.sh contains paths to reference files and project info etc.
chr is the current chromosome number. \n\n"

if test -z $3; then
	echo -e $USAGE
	exit
fi

#Input data
NORMAL=$1
TUMOR=$2
CONFIGFILE=$3

#Modules
module load bioinfo-tools
module load GATK
module load vcftools
module load tabix
set -euo pipefail

#Import config file:
. $CONFIGFILE

#path to results and temp folders
normalprefix=${NORMAL%".md.real.recal.bam"}
tumorprefix=${TUMOR%".md.real.recal.bam"}
SPAIR=$normalprefix"_"$tumorprefix

#MuTect2
#Analyze each chr separately
logdir="mutect2_log"
mkdir -p $logdir
joblist_mutect2=""
mergelist_mutect2=""
cleanlist_mutect2=""
for chr in `seq 1 22` X Y
do
mutect2=$(sbatch -A $PROJECTID -p core -n 4 -t 240:00:00 -J $SPAIR"_mutect2_"$chr -e "$logdir/mutect2_"$chr".err" -o "$logdir/mutect2_"$chr".out" $SCRIPTSDIR"/"run_mutect2_chr.sh $NORMAL $TUMOR $CONFIGFILE $chr &)
IFS='Sumbitted batch job ' read -a jobid_mutect2 <<< $mutect2
joblist_mutect2=$joblist_mutect2${jobid_mutect2[0]}:
mergelist_mutect2=$mergelist_mutect2" $SPAIR.mutect2.$chr.vcf.gz"
cleanlist_mutect2=$cleanlist_mutect2" $SPAIR.mutect2.$chr.vcf.gz $SPAIR.mutect2.$chr.vcf.gz.tbi"
done
joblist_mutect2=${joblist_mutect2%?}
merge=$(sbatch --dependency=afterok:$joblist_mutect2 -A $PROJECTID -p core -t 240:00:00 -J $SPAIR"_merge_chr_vcfs" -e "$logdir/merge_chr_vcfs.err" -o $SPAIR".mutect2.vcf" vcf-concat $mergelist_mutect2)

IFS='Sumbitted batch job ' read -a jobid_merge <<< $merge
clean=$(sbatch --dependency=afterok:${jobid_merge[0]} -A $PROJECTID -p core -t 240:00:00 -J $SPAIR"_clean" -e "$logdir/clean.err" -o "$logdir/clean.out"  $SCRIPTSDIR"/"clean_mutect2.sh $cleanlist_mutect2)



