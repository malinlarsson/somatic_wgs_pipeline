#! /bin/bash

USAGE="\nRealign tumor and normal together round indels. \n

Usage: <sbatch syntax> $0 normal.bam tumor.bam  configfile\n\n

Where\n 
<sbatch syntax> is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p node -n 16  -t 24:00:00 -J realign
-e somename.err -o somename.out\n\n
and
configfile contains paths to reference files etc. \n"

if test -z $3; then
	echo -e $USAGE
	exit
fi

#Input data
NBAM=$1
TBAM=$2
CONFIGFILE=$3


#Modules
module load bioinfo-tools
module load GATK
set -euo pipefail

#Reading config file
. $CONFIGFILE

prefixnormal=${NBAM%".bam"}
prefixtumor=${TBAM%".bam"}
echo "prefix normal: "$prefixnormal
echo "prefix tumor: "$prefixtumor
SPAIR=$prefixnormal"_"$prefixtumor
echo "spair"
################################################
#
# Realignment (GATK)
#
###############################################


echo "#######  Realigner Target Creator (GATK)"
java -jar $GATK_HOME/GenomeAnalysisTK.jar \
-T RealignerTargetCreator \
-R $REFERENCE \
-I $NBAM -I $TBAM \
-known $KGINDELS \
-known $MILLS \
-o $SPAIR".intervals" \
-nt 16
if test $? = 0;then
    echo "RealignerTargetCreator went well!"
fi
wait

echo "#######  Indel realigner (GATK)"
java -jar $GATK_HOME/GenomeAnalysisTK.jar \
-T IndelRealigner \
-R $REFERENCE \
-I $NBAM -I $TBAM \
-targetIntervals $SPAIR".intervals" \
-known $KGINDELS \
-known $MILLS \
-nWayOut '.real.bam'

#cleanup
if test $? = 0;then
    echo "IndelRealigner went well!"
    rm $NBAM $TBAM
    #remove index for old normal.bam
    prefix_normal=${NBAM%".bam"}
    if [ -f $prefix_normal".bai" ]; then
        rm $prefix_normal".bai"
    elif [ -f $prefix_normal".bam.bai" ]; then
        rm $prefix_normal".bam.bai"
    fi
    #remove index for old tumor.bam
    prefix_tumor=${TBAM%".bam"}
    if [ -f $prefix_tumor".bai" ]; then
        rm $prefix_tumor".bai"
    elif [ -f $prefix_tumor".bam.bai" ]; then
        rm $prefix_tumor".bam.bai"
    fi
fi

