#! /usr/bin/env python

import sys, re, math, random
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages

if len(sys.argv)<1:
    print "Usage: %s sample_name MuTect1_vcf MuTect2_vcf Strelka_vcf \n" %sys.argv[0]
    sys.exit(0)


def main():
    sample = sys.argv[1]
    mutect1_vcf = sys.argv[2]
    mutect2_vcf = sys.argv[3]
    strelka_vcf= sys.argv[4]
    #of_snvs=open(s nvs, 'w')
    #of_indels=open(indels, 'w')
    mutect2=parse_mutect2(mutect2_vcf)
    mutect1=parse_mutect1(mutect1_vcf)
    strelka=parse_strelka_snvs(strelka_vcf)


#generate_output(mutect1, mutect2, strelka, sample)
    plot_allele_freqs(mutect1, mutect2, strelka, sample)

#def annotate(sample):

def plot_allele_freqs(mutect1, mutect2, strelka, sample):
    columns =  ['MuTect1','MuTect2', 'Strelka', 'M1M2I', 'M1SI','M2SI','M1M2SI' ]

    allele_freq=np.empty(7)
    allele_freq[:] = np.NAN

    all_snvs=mutect1['snvs'].keys()+mutect2['snvs'].keys()+strelka['snvs'].keys()
    antal=0
    for pos in all_snvs:

        #Format the AF row for each position
        #Format 'M1S','M2S', 'SS', 'M1M2I', 'M1SI','M2SI','M1M2SI'
        this_variant=np.empty(7)
        this_variant[:]=np.NAN
        vcfinfo = {}
        #Which caller(s) detected the variant?
        if pos in mutect1['snvs']:
            vcfinfo['mutect1']=mutect1['snvs'][pos]['info']
        if pos in mutect2['snvs']:
            vcfinfo['mutect2']=mutect2['snvs'][pos]['info']
        if pos in strelka['snvs']:
            vcfinfo['strelka']=strelka['snvs'][pos]['info']
        called_by=vcfinfo.keys()
        if all(value == vcfinfo[called_by[0]] for value in vcfinfo.values()):


            if pos in mutect1['snvs'] and pos not in mutect2['snvs'] and pos not in strelka['snvs']:
                this_variant[0]=float(mutect1['snvs'][pos]['ad']['tumor'].split(",")[1])/(float(mutect1['snvs'][pos]['ad']['tumor'].split(",")[0])+float(mutect1['snvs'][pos]['ad']['tumor'].split(",")[1]))
                print this_variant
                allele_freq=np.vstack((allele_freq, this_variant))
            elif pos in mutect2['snvs'] and pos not in mutect1['snvs'] and pos not in strelka['snvs']:
                this_variant[1]=float(mutect1['snvs'][pos]['ad']['tumor'].split(",")[1])/(float(mutect1['snvs'][pos]['ad']['tumor'].split(",")[0])+float(mutect1['snvs'][pos]['ad']['tumor'].split(",")[1]))
                print this_variant
                allele_freq=np.vstack((allele_freq, this_variant))
            elif pos in strelka['snvs'] and pos not in mutect1['snvs'] and pos not in mutet2['snvs']:
                this_variant[3]=float(mutect1['snvs'][pos]['ad']['tumor'].split(",")[1])/(float(mutect1['snvs'][pos]['ad']['tumor'].split(",")[0])+float(mutect1['snvs'][pos]['ad']['tumor'].split(",")[1]))
                print this_variant
                allele_freq=np.vstack((allele_freq, this_variant))
            

        #include this_site_freqs in allele_freq matrix
        #allele_freq=np.vstack((allele_freq, this_site_freqs))


##Create plots and print to PDF file
#pp = PdfPages(out+'.pdf')
##Boxplot
#x=range(1, len(sample_name)+1)
#plt.boxplot(allele_freq)
#plt.xticks(x, sample_name, rotation=45)
#plt.title('Alt allele frequency distributions in '+vcffile+'\n')
#plt.ylabel('Alt allele frequency based on read depths')
#plt.margins(0.1)
#plt.subplots_adjust(bottom=0.3)
#plt.savefig(pp, format='pdf')
#plt.clf()
#
##Histogram
#n, bins,patches=plt.hist(allele_freq, label=sample_name)
#plt.legend()
#plt.xlabel('Alt allele freq based on read depths')
#plt.ylabel('Counts (SNVs)')
#plt.title('Alt allele frequency distributions in '+vcffile+'\n')
#plt.savefig(pp, format='pdf')
##plt.show()
#pp.close()
#
#print 'printed results to '+out+'.pdf'



def generate_output(mutect1, mutect2, strelka, sample):
    snv_file=sample+'_snvs.txt'
    avinput=sample+'.avinput'
    sf = open(snv_file, 'w')
    ai = open(avinput, 'w')
    sf.write("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" %('#CHROM', 'POS', 'REF', 'ALT', 'CALLED_BY', 'FORMAT', sample+'_tumor', sample+'_normal'))
    #All mutated snvs:
    all_snvs=mutect1['snvs'].keys()+mutect2['snvs'].keys()+strelka['snvs'].keys()
    antal=0
    for pos in all_snvs:
        vcfinfo = {}
        #Which caller(s) detected the variant?
        if pos in mutect1['snvs']:
            vcfinfo['mutect1']=mutect1['snvs'][pos]['info']
        if pos in mutect2['snvs']:
            vcfinfo['mutect2']=mutect2['snvs'][pos]['info']
        if pos in strelka['snvs']:
            vcfinfo['strelka']=strelka['snvs'][pos]['info']
        called_by=vcfinfo.keys()

        #Do we have the same basic info from all callers? Should be...
        if all(value == vcfinfo[called_by[0]] for value in vcfinfo.values()):
            format=''
            gf_tumor=''
            gf_normal=''
            callers=''
            for c in called_by:
                if c=='mutect1':
                    callers=callers+'MuTect1,'
                    format=format+'ADM1:'
                    gf_tumor=gf_tumor+mutect1['snvs'][pos]['ad']['tumor']+':'
                    gf_normal=gf_normal+mutect1['snvs'][pos]['ad']['normal']+':'
                elif c=='mutect2':
                    callers=callers+'MuTect2,'
                    format=format+'ADM2:'
                    gf_tumor=gf_tumor+mutect2['snvs'][pos]['ad']['tumor']+':'
                    gf_normal=gf_normal+mutect2['snvs'][pos]['ad']['normal']+':'
                elif c=='strelka':
                    callers=callers+'Strelka,'
                    format=format+'ADS:'
                    gf_tumor=gf_tumor+strelka['snvs'][pos]['ad']['tumor']+':'
                    gf_normal=gf_normal+strelka['snvs'][pos]['ad']['normal']+':'
            callers = callers[:-1]
            format = format[:-1]
            gf_tumor = gf_tumor[:-1]
            gf_normal = gf_normal[:-1]
            antal = antal+1
            sf.write("%s\t%s\t%s\t%s\t%s\n" %(vcfinfo[called_by[0]],callers, format, gf_tumor, gf_normal))
            ai.write("%s\n" %(vcfinfo[called_by[0]]))
        else:
            print "Conflict in ref and alt alleles between callers "+called_by+" at pos "+pos




def parse_mutect2(vcf):
    snvs = {}
    indels = {}
    for line in open(vcf, 'r'):
        line=line.strip()
        if not line.startswith("#"):
            filter1=re.compile('alt_allele_in_normal')
            filter2=re.compile('clustered_events')
            filter3=re.compile('germline_risk')
            filter4=re.compile('homologous_mapping_event')
            filter5=re.compile('multi_event_alt_allele_in_normal')
            filter6=re.compile('panel_of_normals')
            filter7=re.compile('str_contraction')
            filter8=re.compile('t_lod_fstar')
            filter9=re.compile('triallelic_site')
            f1=filter1.search(line)
            f2=filter2.search(line)
            f3=filter3.search(line)
            f4=filter4.search(line)
            f5=filter5.search(line)
            f6=filter6.search(line)
            f7=filter7.search(line)
            f8=filter8.search(line)
            f9=filter9.search(line)
            if not (f1 or f2 or f3 or f4 or f5 or f6 or f7 or f8 or f9):
                info=line.split("\t")
                pos=info[0]+'_'+info[1]
                vcfinfo=info[0]+'\t'+info[1]+'\t'+info[3]+'\t'+info[4]
                ad_tumor=info[9].split(":")[1]
                ad_normal=info[10].split(":")[1]
                ref=info[3]
                alt=info[4]
                #Indels
                if len(ref)>1 or len(alt)>1:
                    indels[pos] = {}
                    indels[pos]['info']=vcfinfo
                    indels[pos]['ad'] = {}
                    indels[pos]['ad']['tumor']=ad_tumor
                    indels[pos]['ad']['normal']=ad_normal
                #snvs
                else:
                    snvs[pos] = {}
                    snvs[pos]['info']=vcfinfo
                    snvs[pos]['ad'] = {}
                    snvs[pos]['ad']['tumor']=ad_tumor
                    snvs[pos]['ad']['normal']=ad_normal
    return {'indels':indels,'snvs':snvs}


def parse_mutect1(vcf):
    snvs = {}
    for line in open(vcf, 'r'):
        line=line.strip()
        if not line.startswith("#"):
            filter1=re.compile('REJECT')
            f1=filter1.search(line)
            if not (f1):
                info=line.split("\t")
                pos=info[0]+'_'+info[1]
                vcfinfo=info[0]+'\t'+info[1]+'\t'+info[3]+'\t'+info[4]
                ad_tumor=info[9].split(":")[1]
                ad_normal=info[10].split(":")[1]
                snvs[pos] = {}
                snvs[pos]['info']=vcfinfo
                snvs[pos]['ad'] = {}
                snvs[pos]['ad']['tumor']=ad_tumor
                snvs[pos]['ad']['normal']=ad_normal
    return {'snvs':snvs}

def parse_strelka_snvs(vcf):
    snvs = {}
    for line in open(vcf, 'r'):
        line=line.strip()
        if not line.startswith("#"):
            info=line.split("\t")
            pos=info[0]+'_'+info[1]
            vcfinfo=info[0]+'\t'+info[1]+'\t'+info[3]+'\t'+info[4]
            ref=info[3]
            alt=info[4]
            ad_normal = {}
            ad_tumor = {}
            #Using tiers 2 data
            ad_tumor['A']=int(info[10].split(":")[4].split(",")[1])
            ad_tumor['C']=int(info[10].split(":")[5].split(",")[1])
            ad_tumor['G']=int(info[10].split(":")[6].split(",")[1])
            ad_tumor['T']=int(info[10].split(":")[7].split(",")[1])
            ad_normal['A']=int(info[9].split(":")[4].split(",")[1])
            ad_normal['C']=int(info[9].split(":")[5].split(",")[1])
            ad_normal['G']=int(info[9].split(":")[6].split(",")[1])
            ad_normal['T']=int(info[9].split(":")[7].split(",")[1])
            snvs[pos] = {}
            snvs[pos]['info']=vcfinfo
            snvs[pos]['ad'] = {}
            snvs[pos]['ad']['tumor']=str(ad_tumor[ref])+','+str(ad_tumor[alt])
            snvs[pos]['ad']['normal']=str(ad_normal[ref])+','+str(ad_normal[alt])
    return {'snvs':snvs}



main()
