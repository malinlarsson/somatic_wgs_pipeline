#! /usr/bin/env python

import sys, re


if len(sys.argv)<3:
    print "Usage: %s <BAF> <LogR> <loci_subset> <BAF_out> <LogR_out>\n" %sys.argv[0]
    sys.exit(0)


baf = sys.argv[1]
logr = sys.argv[2]
locifile = sys.argv[3]
bafout = sys.argv[4]
logrout = sys.argv[5]

bo = open(bafout, 'w')
lo = open(logrout, 'w')

SNPs= {}
lf = open(locifile, 'r')
for l in lf:
    l=l.strip()
    linfo=l.split()
    id=linfo[0]
    #print id
    SNPs[id]=1

bf = open(baf, 'r')
bo.write("ID\tChr\tPosition\tBAF\n")
alreadyprinted={}
for b in bf:
    b=b.strip()
    binfo=b.split()
    id=binfo[1]+'_'+binfo[2]
    if id in SNPs:
        #print pos
        if id not in alreadyprinted:
            bo.write("%s\t%s\t%s\t%s\n" %(id, binfo[1], binfo[2], binfo[3]))
        else:
            print "already printed "+id
        alreadyprinted[id]=1
bo.close()

lf = open(logr, 'r')
lo.write("ID\tChr\tPosition\tLogR\n")
alreadyprinted={}
for l in lf:
    l=l.strip()
    linfo=l.split()
    id=linfo[1]+'_'+linfo[2]
    if id in SNPs:
        #print pos
        if id not in alreadyprinted:
            lo.write("%s\t%s\t%s\t%s\n" %(id, linfo[1], linfo[2], linfo[3]))
        else:
            print "already printed "+id
        alreadyprinted[id]=1
lo.close()