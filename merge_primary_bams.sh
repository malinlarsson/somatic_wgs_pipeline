#! /bin/bash

USAGE="\n

Usage: <sbatch syntax> $0 SAMPLE\n\n

Where\n
<sbatch syntax> is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p node -n 16  -t 24:00:00 -J merge
-e somename.err -o somename.out\n
SAMPLEID = Sampleid\n\n"

if test -z $1; then
echo -e $USAGE
exit
fi

#Modules
module load bioinfo-tools
module load picard
module load samtools
set -euo pipefail

#Input
SAMPLE=$1


###############################################
#
# Merge all .bams in current folder for sample
#
###############################################

FILECOUNT=$(ls "$SAMPLE"*".bam" |wc -l)
echo "Number of files: "$FILECOUNT

#Here is the code for merging bam files with picard
options=""
cleaninglist=""
for bam in "$SAMPLE"*".bam"
do
    options=$options"INPUT="$bam" "
    cleaninglist=$cleaninglist" "$bam
done
options=$options"OUTPUT="$SAMPLE".bam SORT_ORDER=coordinate VALIDATION_STRINGENCY=LENIENT CREATE_INDEX=true"

if test $FILECOUNT -gt 1;then
    java -jar $PICARD_HOME"/MergeSamFiles.jar" $options
    if test $? = 0;then
        echo "Bam files merged with picard"
        rm $cleaninglist
    fi
else
    mv "$SAMPLE"*".bam" $SAMPLE".bam"
    #samtools index $SAMPLE".bam"
    if test $? = 0;then
        java -jar $PICARD_HOME"/BuildBamIndex.jar" INPUT=$SAMPLE".bam"
        echo "Primary bam file moved to $SAMPLE.bam"
    fi
fi
