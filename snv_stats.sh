#! /bin/bash

USAGE="\n

Usage: <sbatch syntax> $0 sample mutect1.vcf mutect2.vcf strelkasnvs.vcf strelkaindels.vcf configfile\n"

if test -z $4; then
echo -e $USAGE
exit
fi

#Modules
module load bioinfo-tools
module load BEDTools
set -euo pipefail


#Input
sample=$1
mutect1=$2
mutect2=$3
strelkasnvs=$4
strelkaindels=$5
configfile=$6

#Load configfile
. $configfile


#
# Filter MuTect1 calls
#
grep -v 'REJECT' $mutect1 > $sample".mutect1.filtered.vcf"


#
# Filter MuTect2 calls
# This creates $sample.mutect2.filtered.snvs.vcf
# and $sample.mutect2.filtered.indels.vcf
#
$SCRIPTSDIR"/"filter_mutect2.py $sample $mutect2


#
# Strelka already filtered
#

#
# Now looking for overlap
#

grep '#' $sample".mutect1.filtered.vcf" > $sample".mutect1.mutect2.snvs.vcf"
bedtools intersect -a $sample".mutect1.filtered.vcf" -b $sample".mutect2.filtered.snvs.vcf"  -sorted -g $BEDTOOLS_GENOME >> $sample".mutect1.mutect2.snvs.vcf"

grep '#' $sample".mutect1.filtered.vcf" > $sample".mutect1.strelka.snvs.vcf"
bedtools intersect -a $sample".mutect1.filtered.vcf" -b $strelkasnvs -sorted  -g $BEDTOOLS_GENOME >> $sample".mutect1.strelka.snvs.vcf"

grep '#' $sample".mutect2.filtered.snvs.vcf" > $sample".mutect2.strelka.snvs.vcf"
bedtools intersect -a $sample".mutect2.filtered.snvs.vcf" -b $strelkasnvs -sorted -g $BEDTOOLS_GENOME >> $sample".mutect2.strelka.snvs.vcf"

grep '#' $sample".mutect1.mutect2.snvs.vcf" > $sample".mutect1.mutect2.strelka.snvs.vcf"
bedtools intersect -a $sample".mutect1.mutect2.snvs.vcf" -b $strelkasnvs -sorted -g $BEDTOOLS_GENOME >> $sample".mutect1.mutect2.strelka.snvs.vcf"

m1=$(grep -v '#' "$sample.mutect1.filtered.vcf" | wc)
m2=$(grep -v '#' "$sample.mutect2.filtered.snvs.vcf" | wc)
s=$(grep -v '#' $strelkasnvs | wc)
m1_vs_m2=$(grep -v '#' "$sample.mutect1.mutect2.snvs.vcf" | wc)
m1_vs_s=$(grep -v '#' "$sample.mutect1.strelka.snvs.vcf" | wc)
m2_vs_s=$(grep -v '#' "$sample.mutect2.strelka.snvs.vcf" | wc)
m1_m2_s=$(grep -v '#' $sample".mutect1.mutect2.strelka.snvs.vcf" | wc)

echo "MuTect1 "$m1
echo "MuTect2 "$m2
echo "Strelka "$s
echo "MuTect1 vs MuTect2 "$m1_vs_m2
echo "MuTect1 vs Strelka "$m1_vs_s
echo "MuTect2 vs Strelka "$m2_vs_s
echo "MuTect1 vs MuTect2 vs strelka "$m1_m2_s


