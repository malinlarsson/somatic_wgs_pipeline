#! /bin/bash

USAGE="\n

Usage: <sbatch syntax> $0 sample mutect1.vcf mutect2.vcf strelkasnvs.vcf strelkaindels.vcf configfile outdir\n"

if test -z $4; then
echo -e $USAGE
exit
fi

#Modules
module load bioinfo-tools
module load BEDTools
set -euo pipefail


#Input
sample=$1
mutect1=$2
mutect2=$3
strelkasnvs=$4
strelkaindels=$5
configfile=$6
outdir=$7

#Load configfile
. $configfile

basedir=$PWD
echo "basedir "$basedir

mkdir -p $outdir
cd $outdir

#
# Filter MuTect1 calls
#

echo $mutect1
grep -v 'REJECT' $basedir"/"$mutect1 > $sample".mutect1.filtered.vcf"


#
# Filter MuTect2 calls
# This creates $sample.mutect2.filtered.snvs.vcf
# and $sample.mutect2.filtered.indels.vcf
#
$SCRIPTSDIR"/"filter_mutect2.py $sample $basedir"/"$mutect2


#
# Strelka already filtered
#

#
# Now looking for overlap
#

grep '#' $sample".mutect1.filtered.vcf" > $sample".mutect1.mutect2.snvs.vcf"
bedtools intersect -a $sample".mutect1.filtered.vcf" -b $sample".mutect2.filtered.snvs.vcf"  -sorted -g $BEDTOOLS_GENOME >> $sample".mutect1.mutect2.snvs.vcf"

grep '#' $sample".mutect1.filtered.vcf" > $sample".mutect1.strelka.snvs.vcf"
bedtools intersect -a $sample".mutect1.filtered.vcf" -b $basedir"/"$strelkasnvs -sorted  -g $BEDTOOLS_GENOME >> $sample".mutect1.strelka.snvs.vcf"

grep '#' $sample".mutect2.filtered.snvs.vcf" > $sample".mutect2.strelka.snvs.vcf"
bedtools intersect -a $sample".mutect2.filtered.snvs.vcf" -b $basedir"/"$strelkasnvs -sorted -g $BEDTOOLS_GENOME >> $sample".mutect2.strelka.snvs.vcf"

grep '#' $sample".mutect1.mutect2.snvs.vcf" > $sample".mutect1.mutect2.strelka.snvs.vcf"
bedtools intersect -a $sample".mutect1.mutect2.snvs.vcf" -b $basedir"/"$strelkasnvs -sorted -g $BEDTOOLS_GENOME >> $sample".mutect1.mutect2.strelka.snvs.vcf"


#Annotate with annovar
#First format files to annovar input format:
grep -v '#' $sample".mutect1.filtered.vcf" | awk '{ print $1, $2, $2, $4, $5}' > $sample".mutect1.avinput"
grep -v '#' $sample".mutect2.filtered.snvs.vcf" | awk '{ print $1, $2, $2, $4, $5}' > $sample".mutect2.snvs.avinput"
grep -v '#' $basedir"/"$strelkasnvs | awk '{ print $1, $2, $2, $4, $5}' > $sample".strelka.snvs.avinput"

grep -v '#' $sample".mutect1.mutect2.snvs.vcf" | awk '{ print $1, $2, $2, $4, $5}' > $sample".mutect1.mutect2.snvs.avinput"
grep -v '#' $sample".mutect1.strelka.snvs.vcf" | awk '{ print $1, $2, $2, $4, $5}' > $sample".mutect1.strelka.snvs.avinput"
grep -v '#' $sample".mutect2.strelka.snvs.vcf" | awk '{ print $1, $2, $2, $4, $5}' > $sample".mutect2.strelka.snvs.avinput"
grep -v '#' $sample".mutect1.mutect2.strelka.snvs.vcf" | awk '{ print $1, $2, $2, $4, $5}' > $sample".mutect1.mutect2.strelka.snvs.avinput"





#Annotate with annovar, fullösning
/home/malin/glob/annovar/annotate_variation.pl -out $sample".mutect1.filtered" -build hg19 $sample".mutect1.avinput" /home/malin/glob/annovar/humandb/
/home/malin/glob/annovar/annotate_variation.pl -out $sample".mutect2.snvs.filtered" -build hg19 $sample".mutect2.snvs.avinput" /home/malin/glob/annovar/humandb/
/home/malin/glob/annovar/annotate_variation.pl -out $sample".strelka.snvs" -build hg19 $sample".strelka.snvs.avinput" /home/malin/glob/annovar/humandb/

/home/malin/glob/annovar/annotate_variation.pl -out $sample".mutect1.mutect2.snvs" -build hg19 $sample".mutect1.mutect2.snvs.avinput" /home/malin/glob/annovar/humandb/
/home/malin/glob/annovar/annotate_variation.pl -out $sample".mutect1.strelka.snvs" -build hg19 $sample".mutect1.strelka.snvs.avinput" /home/malin/glob/annovar/humandb/
/home/malin/glob/annovar/annotate_variation.pl -out $sample".mutect2.strelka.snvs" -build hg19 $sample".mutect2.strelka.snvs.avinput" /home/malin/glob/annovar/humandb/
/home/malin/glob/annovar/annotate_variation.pl -out $sample".mutect1.mutect2.strelka.snvs" -build hg19 $sample".mutect1.mutect2.strelka.snvs.avinput" /home/malin/glob/annovar/humandb/




m1=$(grep -v '#' "$sample.mutect1.filtered.vcf" | wc)
m2=$(grep -v '#' "$sample.mutect2.filtered.snvs.vcf" | wc)
s=$(grep -v '#' $basedir"/"$strelkasnvs | wc)
m1_m2=$(grep -v '#' "$sample.mutect1.mutect2.snvs.vcf" | wc)
m1_s=$(grep -v '#' "$sample.mutect1.strelka.snvs.vcf" | wc)
m2_s=$(grep -v '#' "$sample.mutect2.strelka.snvs.vcf" | wc)
m1_m2_s=$(grep -v '#' $sample".mutect1.mutect2.strelka.snvs.vcf" | wc)

m1_ns=$(grep 'nonsynonymous' "$sample.mutect1.filtered.exonic_variant_function" | wc)
m2_ns=$(grep 'nonsynonymous' "$sample.mutect2.snvs.filtered.exonic_variant_function" | wc)
s_ns=$(grep 'nonsynonymous' "$sample.strelka.snvs.exonic_variant_function" | wc)

m1_m2_ns=$(grep 'nonsynonymous' "$sample.mutect1.mutect2.snvs.exonic_variant_function" | wc)
m1_s_ns=$(grep 'nonsynonymous' "$sample.mutect1.strelka.snvs.exonic_variant_function" | wc)
m2_s_ns=$(grep 'nonsynonymous' "$sample.mutect2.strelka.snvs.exonic_variant_function" | wc)
m1_m2_s_ns=$(grep 'nonsynonymous' "$sample.mutect1.mutect2.strelka.snvs.exonic_variant_function" | wc)




echo "SNVs:" > $sample.snvstats.txt
echo "MuTect1 "$m1 >> $sample.snvstats.txt
echo "MuTect2 "$m2 >> $sample.snvstats.txt
echo "Strelka "$s >> $sample.snvstats.txt
echo "MuTect1 intersect MuTect2 "$m1_m2 >> $sample.snvstats.txt
echo "MuTect1 intersect Strelka "$m1_s >> $sample.snvstats.txt
echo "MuTect2 intersect Strelka "$m2_s >> $sample.snvstats.txt
echo "MuTect1 intersect MuTect2 intersect strelka "$m1_m2_s >> $sample.snvstats.txt
echo "Nonsynonymous aa changes:" >> $sample.snvstats.txt
echo "MuTect1 "$m1_ns >> $sample.snvstats.txt
echo "MuTect2 "$m2_ns >> $sample.snvstats.txt
echo "Strelka "$s_ns >> $sample.snvstats.txt

echo "MuTect1 intersect MuTect2 "$m1_m2_ns >> $sample.snvstats.txt
echo "MuTect1 intersect Strelka "$m1_s_ns >> $sample.snvstats.txt
echo "MuTect2 intersect Strelka "$m2_s_ns >> $sample.snvstats.txt
echo "MuTect1 intersect MuTect2 intersect Strelka "$m1_m2_s_ns >> $sample.snvstats.txt





