# Pipeline for somatic variant detection in matched tumor-normal wgs data
By Malin Larsson, SciLifeLab Bioinformatics Long-term Support (WABI)
malin.larsson@scilifelab.se

## Overview
This is a pipeline for detecting somatic single base mutations and somatic indels in matched tumor normal samples. The pipeline is developed for usage on the HPC cluster UPPMAX (http://www.uppmax.uu.se). 
Note: the pipeline is under development 


Preparation
Raw data should be collected in one main folder with subfolders for each sample in the cohort. The subfolders should be named as the sample IDs. For each sample there can be one or many fastq files.  In the test cohort, there were at least one set of paired-end reads (i.e. two fastq files), for each sample. 

Below is an example of the structure of the datadir:  
+-- sample1  
    +-- s_2_1_sequence.fastq.gz  
    +-- s_2_2_sequence.fastq.gz  
    +-- s_6_1_sequence.fastq.gz  
    +-- s_6_2_sequence.fastq.gz  
+-- sample2  
    +-- s_1_1_sequence.fastq.gz  
    +-- s_1_2_sequence.fastq.gz  
+-- sample3  
    +-- s_1_1_sequence.fastq.gz  
    +-- s_1_2_sequence.fastq.gz  


Sequence files are mapped to the reference genome using the BWA mem -M  
All bam files for a sample are merged with Picard MergeSamFiles  
The merged bam file deduplicated using Picard MarkDuplicates  
Indel realignment is applied jointly to the tumor and normal bam files using GATK  
The base quality scores of the bam file are recalibrated using GATK  
SNV calling is done with Freebayes, MutTect1, MuTect2 (should be omitted?) and Streak.   
An identity check is made to make sure that the normal and tumor samples are from the same individuals.    
To be added soon: SV calling with Manta and CNV calling with ASCAT.
   
*run_all.sh* calls the individual analyses steps in separate sbatch jobs, with dependencies between jobs when needed. All independent analysis steps are performed in parallell. All output is written to the file system (to the directory from which the script was started)  
run_all.sh is started as a normal bash script with tree input parameters:  
run_all.sh Normal_sampleid Tumor_sampleid /full/path/to/configfile.sh    
![Overview of workflow](run_all.png "Overview of workflow")
  

## Running the pipeline

1)  All pipeline script files should be placed in the same folder, referred to as *scriptsdir*

2) Modify project-specific info in configfile.  
The path to *scriptsdir* should be set to a folder in which the scripts are stored (probably .../somatic_wgs_pipeline).  
The path to *datadir* should be set to the main data folder described above.  
The FASTQ_SUFFIX should be adjusted to fit the format of the fastq file names in project. In above example, FASTQ_SUFFIX should be *_sequence.fastq.gz* i.e the part of the file names that is identical for all sammples.
 
3) Create a working directory, for example *pipeline_test* and move to that directory.
$ mkdir pipeline_test
$ cd pipeline_test

4) Start the analysis of individual normal - tumor sample pairs with this command:     
$ /path/to/scriptsdir/run_all.sh normal_id matched_tumor_id /full/path/to/configfile.sh  
  
(or if you want to run everything in one job, although this is not recommended   
$sbatch -A bXXXXXXX -p node -n 16 -t 240:00:00 -J *somejobname* -e *somefilename.err* -o *somefilename.out* /path/to/scriptsdir/run_all_one_job.sh normal_id matched_tumor_id /full/path/to/configfile.sh)


## Output
A separate folder is created for each tumor/normal sample pair, named *normal_id_tumor_id*  
The results folders contain the following files:  
*QC metrics:*  
normal.md_metrics  
tumor.md_metrics  
normalid.md_tumorid.md.intervals  
normal.md.real.table  
normal.md.real.recal.table  
normal.md.real.recal_plots.pdf  
tumor.md.real.table  
tumor.md.real.recal.table  
tumor.md.real.recal_plots.pdf  
*Results:*  
normal.md.real.recal.bam  
tumor.md.real.recal.bam  
normalid_tumorid.mutect1.vcf  
normalid_tumorid.mutect2.vcf  
normalid.freebayes.vcf  
normalid_tumorid.freebayes.vcf   
normalid_tumorid.heterozygoteConcordance   
directory "strelka" with a subdirectory "results" that contains the streaka .vcf files  
Output from Manta and ASCAT - to be described here!
Also, log files with extensions .out and .err are created for each sbatch when starting the pipeline using *run_all.sh*