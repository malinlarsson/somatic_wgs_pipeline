#! /bin/bash

USAGE="\n

Usage: <sbatch syntax> $0 sample.bam configfile.sh out.allelecount\n\n

Where\n
<sbatch syntax> is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p core -n 1  -t 24:00:00 -J jobname
-e somename.err -o somename.out\n
and\n
configfile.sh contains paths to reference files and project info etc.  \n\n"

if test -z $3; then
echo -e $USAGE
exit
fi

#Input data
BAM=$1
CONFIGFILE=$2
OUT=$3
sample=${BAM%".md.real.recal.bam"}
set -euo pipefail

#Modules
module load bioinfo-tools
module load samtools
set -euo pipefail

#Reading config file
. $CONFIGFILE

if [ ! -f "$BAM" ]; then
echo "Bamfile not found!"
exit 1
fi

alleleCounter -l $LOCIFILE -r $REFERENCE -b $BAM -o $OUT
if test $? = 0;then
echo "HC test went well!"
fi
