#! /bin/bash

USAGE="\n

Usage: <sbatch syntax> $0 normal tumor configfile.sh \n\n

Where\n
<sbatch syntax> is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p node -n 16  -t 24:00:00 -J bam2vcf
-e somename.err -o somename.out\n
and\n
normal = sample id of normal sample
tumor = sample id of normal sample
configfile.sh contains paths to reference files and project info etc.  \n\n"

if test -z $3; then
echo -e $USAGE
exit
fi

#Input data
NORMAL=$1
TUMOR=$2
CONFIGFILE=$3
SPAIR=$NORMAL"_"$TUMOR

set -euo pipefail

#Reading config file
. $CONFIGFILE

if [ ! -f "$NORMAL.freebayes.vcf" ]; then
echo "File $NORMAL.freebayes.vcf not found!"
exit 1
fi

if [ ! -f "$TUMOR.md.real.recal.bam" ]; then
echo "File $TUMOR.md.real.recal.bam not found!"
exit 1
fi

echo "normal sample: "$NORMAL
echo "tumor sample: "$TUMOR
echo "Sample pair "$SPAIR


java -jar $SCRIPTSDIR/GenomeAnalysisTK-Klevebring.jar \
-T HeterozygoteConcordance \
-V $NORMAL".freebayes.vcf" \
-sid $NORMAL \
-I $TUMOR".md.real.recal.bam" \
-R $REFERENCE \
-L $NORMAL".freebayes.vcf" \
-o $SPAIR".heterozygoteConcordance"
if test $? = 0;then
echo "HC test went well!"
fi
