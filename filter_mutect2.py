#! /usr/bin/env python

import sys, re, math, random
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages

if len(sys.argv)<1:
    print "Usage: %s sample vcf \n" %sys.argv[0]
    sys.exit(0)

sample = sys.argv[1]
invcf = sys.argv[2]

snvs=sample+".mutect2.filtered.snvs.vcf"
indels=sample+".mutect2.filtered.indels.vcf"


of_snvs=open(snvs, 'w')
of_indels=open(indels, 'w')

#meta data
for line in open(invcf, 'r'):
    line=line.strip()
    if line.startswith("#"):
        of_snvs.write("%s\n" %line)
        of_indels.write("%s\n" %line)

    else:
        filter1=re.compile('alt_allele_in_normal')
        filter2=re.compile('clustered_events')
        filter3=re.compile('germline_risk')
        filter4=re.compile('homologous_mapping_event')
        filter5=re.compile('multi_event_alt_allele_in_normal')
        filter6=re.compile('panel_of_normals')
        filter7=re.compile('str_contraction')
        filter8=re.compile('t_lod_fstar')
        filter9=re.compile('triallelic_site')
        f1=filter1.search(line)
        f2=filter2.search(line)
        f3=filter3.search(line)
        f4=filter4.search(line)
        f5=filter5.search(line)
        f6=filter6.search(line)
        f7=filter7.search(line)
        f8=filter8.search(line)
        f9=filter9.search(line)
            
        if not (f1 or f2 or f3 or f4 or f5 or f6 or f7 or f8 or f9):
            info=line.split("\t")
            ref=info[3]
            alt=info[4]
            if len(ref)>1 or len(alt)>1:
                of_indels.write("%s\n" %line)
            else:
                of_snvs.write("%s\n" %line)