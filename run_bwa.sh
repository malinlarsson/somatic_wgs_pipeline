#! /bin/bash

USAGE="\n\nUsage: sbatch syntax $0 sampleid infile_dir configfile\n\n
Where\n
sbatch syntax is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p node -n 16  -t 24:00:00 -J jobname
-e xxx.err -o xxx.out\n
and\n
sampleid = identifyer of sample
infile_dir = location of  trimmed fastq files for sample(can also be specified to same as untrimmed)\n
Note: The folder should only contain data for current sample
configfile.sh contains paths to reference files etc.  \n\n"

if test -z $2; then
echo -e $USAGE
exit
fi

#Input to script
SAMPLE=$1
INFILE_DIR=$2
CONFIGFILE=$3

#Import config file:
. $CONFIGFILE
ROOTDIR=$PWD

module load bioinfo-tools
module load bwa
module load samtools
module load picard
set -euo pipefail


#########################################################################
#
# Parsing fastq file names
# FASTQ_SUFFIX defined in confifg file
#
########################################################################

declare -A fastqpair
cd $INFILE_DIR
if test $? == 0;then

    echo "now in "$PWD
    echo "fastq suffix "$FASTQ_SUFFIX
    for fastq in *$FASTQ_SUFFIX
        do
        prefix=${fastq%$FASTQ_SUFFIX}
        prefix_mate1=${prefix%1}
        prefix_mate2=${prefix%2}

        #Get rid of potential last "." in prefix, for example if files are called normal.1.fastq etc
        prefix_mate1=${prefix_mate1%.}
        prefix_mate2=${prefix_mate2%.}


        if [[ $prefix_mate1 != $prefix ]];then
            fastqpair[$prefix_mate1]=$DATADIR"/"$SAMPLE"/"$fastq
        elif [[ $prefix_mate2 != $prefix ]];then
            fastqpair[$prefix_mate2]=${fastqpair[$prefix_mate2]}" "$DATADIR"/"$SAMPLE"/"$fastq
        else
            echo "fastq file without mate: "$fastq
            echo "skipping this file"
        fi
    done
fi
wait

#########################################################################
#
# Run BWA, convert sam to bam and add read group
#
##########################################################################

cd $ROOTDIR
echo $PWD
if test $? == 0;then
    group=1
    for i in "${!fastqpair[@]}"
        do
        RGI="@RG\tID:$group\tSM:$SAMPLE\tPL:ILLUMINA\tLB:$i\tPU:$i"
        if [[ "$FASTQ_SUFFIX" == *"bzip2" ]];then
            bwa mem -M -R $RGI -t 16 $REFERENCE <(bunzip2 -c ${fastqpair["$i"]}) > $SAMPLE"_"$i".sam"
            if test $? = 0;then
                echo "BWA mem -M completed for fastq files with prefix $i "
            fi
        else
            bwa mem -M -R $RGI -t 16 $REFERENCE ${fastqpair["$i"]} > $SAMPLE"_"$i".sam"
            if test $? = 0;then
                echo "BWA mem -M completed for fastq files with prefix $i "
            fi
        fi
        wait

        #Convert to bam and sort
        java -jar $PICARD_HOME"/SortSam.jar" \
        INPUT=$SAMPLE"_"$i".sam" \
        OUTPUT=$SAMPLE"_"$i".bam" \
        SORT_ORDER=coordinate
        if test $? = 0;then
            echo "sam sorted and converted to bam for prefix $i "
            rm $SAMPLE"_"$i".sam"
        fi
        wait
        group=$((group+1))
    done
fi