#! /usr/bin/env python

import sys, re, math, random

infile = "finishedjob_160323_2.txt"

#Extract info from finishedjobinfo outfile.
#The file must be viewed manually first and relevant jobnames are defined below.

#MutTect2

#Mutect2 on ST438 and b37
#jobname="ST438N_ST438T_mutect2"
#submittime="2016-03-22T05:56:01"

#Mutect2 on ST438 and b37_decoys
jobname="ST438N_ST438T_mutect2"
submittime="2016-03-22T05:57:59"

#Mutect2 on ST475 and b37
#jobname="ST475N_ST475T_mutect2"
#submittime="2016-03-21T10:21:27"

#Mutect2 on ST466 and b37
#jobname="ST466N_ST466T_mutect2"
#submittime="2016-03-21T09:50:3"

#Mutect2 on ST451 and b37. Note: Job failed due to a bug in the script AFTER the mutect step, so
#time still reflect the MuTect run. BUT, time should be updated for chr 12 because this particular
#chr was restarted due to that I accidently killed the job.
#jobname="ST451N_ST451T_mutect2"
#submittime="2016-03-18T15:55:"

#MuTect1


#Freebayes


time=0
maxmem=0
nprocs=0
njobs=0
seconds=0

outfile = jobname+submittime+".jobinfo.txt"
of=open(outfile, 'w')
for line in open(infile, 'r'):
    line=line.strip()
    jobid=re.compile(jobname)
    if (jobid.search(line)):
        subt=re.compile(submittime)
        if (subt.search(line)):
            njobs=njobs+1
            info=line.split(" ")
            for i in info:
                id=i.split("=")[0]
                if (id == "jobname"):
                    value=i.split("=")[1]
                    of.write("jobname %s\t" %value)
                if (id == "procs"):
                    value=i.split("=")[1]
                    nprocs=nprocs+int(value)
                if (id == "maxmemory_in_GiB"):
                    value=i.split("=")[1]
                    
                    of.write("maxmem %s\t" %value)
                    if (float(value)>maxmem):
                        maxmem=float(value)
                if (id == "runtime"):
                    value=i.split("=")[1]
                    print "hours "+value
                    of.write("runtime %s\t" %value)
                    time=0
                    if(len(value.split("-"))>1):
                        t=value.split("-")[1].split(":")
                        time=int(t[2])+60*int(t[1])+3600*int(t[0])+int(value.split("-")[0])*86400
                    else:
                        t=value.split("-")[0].split(":")
                        time=int(t[2])+60*int(t[1])+3600*int(t[0])
                    seconds=seconds+time
                    of.write("runtime seconds %s\n" %time)

  
of.write("\nNumber of jobs %s\n" %njobs)
of.write("Maxmem %s\n" %maxmem)
of.write("procs per job %s\n" %(nprocs/njobs))
of.write("Total runtime (hours) %s\n" %(seconds/3600))
of.write("Total core hours %s\n" %((seconds/3600)*(nprocs/njobs)))
