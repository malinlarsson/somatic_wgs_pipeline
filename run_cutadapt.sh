#! /bin/bash

USAGE="\n\nUsage: sbatch syntax $0 sampleid configfile\n\n
Where\n
sbatch syntax is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p core -n 1 -t 72:00:00 -J jobname
-e xxx.err -o xxx.out\n\n"

if test -z $2; then
echo -e $USAGE
exit
fi

module load bioinfo-tools
module load cutadapt/1.9.1

SAMPLE=$1
CONFIGFILE=$2
. $CONFIGFILE
ROOTDIR=$PWD
TMPDIR=$SNIC_TMP




#########################################################################
#
# Parsing fastq file names
# FASTQ_SUFFIX defined in confifg file
#
########################################################################

declare -A fastqpair
cd $DATADIR"/"$SAMPLE
if test $? == 0;then
echo "now in "$PWD
echo "fastq suffix "$FASTQ_SUFFIX
for fastq in *$FASTQ_SUFFIX
do
prefix=${fastq%$FASTQ_SUFFIX}
prefix_r1=${prefix%1}
prefix_r2=${prefix%2}
#Get rid of potential last "." in prefix, for example if files are called normal.1.fastq etc
prefix_r1=${prefix_r1%.}
prefix_r2=${prefix_r2%.}

if [[ $prefix_r1 != $prefix ]];then
fastqpair[$prefix_r1]=$DATADIR"/"$SAMPLE"/"$fastq
elif [[ $prefix_r2 != $prefix ]];then
fastqpair[$prefix_r2]=${fastqpair[$prefix_r2]}" "$DATADIR"/"$SAMPLE"/"$fastq
else
echo "fastq file without mate: "$fastq
echo "skipping this file"
fi
done
fi
wait



#Copy files to tmpdir, run cutadapt and copy the files back to disk.
#cd $ROOTDIR"/"$TMPDIR
cd $TMPDIR
echo "Now in dir "$PWD
if test $? == 0;then
group=1
    for i in "${!fastqpair[@]}"
    do
    echo $i
    echo ${fastqpair[$i]}
    read -r -a names <<< "${fastqpair[$i]}"
#cp ${names[0]} .
#    cp ${names[1]} .

    r1=${names[0]##*/}
    r2=${names[1]##*/}

    r1_prefix=${r1%$FASTQ_SUFFIX}
    r2_prefix=${r2%$FASTQ_SUFFIX}

    cp ${names[0]} $r1
    cp ${names[1]} $r2

    wait
    cutadapt \
    -a AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC \
    -A AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT \
    -o $r1_prefix.trimmed.fastq.gz -p $r2_prefix.trimmed.fastq.gz \
    $r1 $r2
    wait
    cp $r1_prefix.trimmed.fastq.gz $ROOTDIR/.
    cp $r2_prefix.trimmed.fastq.gz $ROOTDIR/.
    done
fi
wait



