#! /bin/bash


USAGE="\ncalls somatic mutations in a tumor/normal sample pair.\n\n
Usage: $0 normal tumor configfile\n
Where\n
normal and tumor are sample ids.\n
and configfile is /full/path/to/configfile.sh"

if test -z $3; then
echo -e $USAGE
exit
fi

module load bioinfo-tools
module load samtools
module load picard
module load bwa
module load vcftools
module load tabix


#Call to script:
#run_all.sh $NORMAL $TUMOR $CONFIGFILE &

#Input to script
NORMAL=$1
TUMOR=$2
CONFIGFILE=$3

#Import config file:
. $CONFIGFILE

#path to results and temp folders
SPAIR=$NORMAL"_"$TUMOR

#TEMPDIR=$SNIC_TMP"/"$SAMPLE
RESDIR=$PWD"/"$SPAIR
TRIMDIR=$RESDIR"/trimmed_fastq"

echo "RESDIR: "$RESDIR
#echo "TEMPDIR: "$TEMPDIR
echo "DATADIR: "$DATADIR


mkdir -p $RESDIR
mkdir -p $TRIMDIR

cd $RESDIR
if test $? == 0;then


#Run cutadapt in trimdir
cd $TRIMDIR
if test $? == 0;then
    mkdir -p $NORMAL
    cd $NORMAL
    if test $= == 0;then
        cutadapt_normal=$(sbatch -A $PROJECTID -p node -n 2 -t 240:00:00 -J "cutadapt_"$NORMAL -e "cutadapt_"$NORMAL".err" -o "cutadapt_"$NORMAL".out" $SCRIPTSDIR"/"run_cutadapt_devel.sh $NORMAL $CONFIGFILE &)
        echo "Calling run_cutadapt.sh for "$NORMAL
        IFS='Sumbitted batch job ' read -a jobid_cutadapt_normal <<< $cutadapt_normal
    fi
    cd ..
    mkdir -p $TUMOR
    cd $TUMOR
    if test $= == 0;then
        cutadapt_tumor=$(sbatch -A $PROJECTID -p node -n 2 -t 240:00:00 -J "cutadapt_"$TUMOR -e "cutadapt_"$TUMOR".err" -o "cutadapt_"$TUMOR".out" $SCRIPTSDIR"/"run_cutadapt_devel.sh $TUMOR $CONFIGFILE &)
        echo "Calling run_cutadapt.sh for "$TUMOR
        IFS='Sumbitted batch job ' read -a jobid_cutadapt_tumor <<< $cutadapt_tumor
    fi
    cd ..
fi
cd ..


#Run bwa
echo "Calling run_bwa.sh for "$NORMAL
bwa_normal=$(sbatch --dependency=afterok:${jobid_cutadapt_normal[0]} -A $PROJECTID -p node -n 16 -t 240:00:00 -J "bwa_"$NORMAL -e "bwa_"$NORMAL".err" -o "bwa_"$NORMAL".out"  $SCRIPTSDIR"/"run_bwa.sh $NORMAL $TRIMDIR"/"$NORMAL $CONFIGFILE &)
echo "Calling run_bwa.sh for "$TUMOR
bwa_tumor=$(sbatch --dependency=afterok:${jobid_cutadapt_tumor[0]} -A $PROJECTID -p node -n 16 -t 240:00:00 -J "bwa_"$TUMOR -e "bwa_"$TUMOR".err" -o "bwa_"$TUMOR".out"  $SCRIPTSDIR"/"run_bwa.sh $TUMOR $TRIMDIR"/"$TUMOR $CONFIGFILE &)
echo "idtext: "$bwa_normal
IFS='Sumbitted batch job ' read -a jobid_bwa_normal <<< $bwa_normal
IFS='Sumbitted batch job ' read -a jobid_bwa_tumor <<< $bwa_tumor

#Merge primary bam files
merge_normal=$(sbatch --dependency=afterok:${jobid_bwa_normal[0]} -A $PROJECTID -p node -n 16 -t 240:00:00 -J "merge_"$NORMAL -e merge_normal.err -o merge_normal.out $SCRIPTSDIR"/"merge_primary_bams.sh $NORMAL &)
merge_tumor=$(sbatch --dependency=afterok:${jobid_bwa_tumor[0]} -A $PROJECTID -p node  -n 16 -t 240:00:00 -J "merge_"$TUMOR -e merge_tumor.err -o merge_tumor.out $SCRIPTSDIR"/"merge_primary_bams.sh $TUMOR &)

IFS='Sumbitted batch job ' read -a jobid_merge_normal <<< $merge_normal
IFS='Sumbitted batch job ' read -a jobid_merge_tumor <<< $merge_tumor

#mark duplicates
md_normal=$(sbatch --dependency=afterok:${jobid_merge_normal[0]} -A $PROJECTID -p node -n 16 -t 240:00:00  -J "md_"$NORMAL -e md_normal.err -o md_normal.out $SCRIPTSDIR"/"run_md.sh $NORMAL".bam" &)
md_tumor=$(sbatch --dependency=afterok:${jobid_merge_tumor[0]} -A $PROJECTID -p node -n 16 -t 240:00:00 -J "md_"$TUMOR -e md_tumor.err -o md_tumor.out $SCRIPTSDIR"/"run_md.sh $TUMOR".bam" &)
IFS='Sumbitted batch job ' read -a jobid_md_normal <<< $md_normal
IFS='Sumbitted batch job ' read -a jobid_md_tumor <<< $md_tumor

#indel realignment
real=$(sbatch --dependency=afterok:${jobid_md_normal[0]}:${jobid_md_tumor[0]} -A $PROJECTID -p node -n 16 -t 240:00:00 -J "real_"$NORMAL"_"$TUMOR -e real.err -o real.out $SCRIPTSDIR"/"run_joint_real.sh $NORMAL".md.bam" $TUMOR".md.bam" $CONFIGFILE &)
IFS='Sumbitted batch job ' read -a jobid_real <<< $real

#base quality score recalibration
recal_normal=$(sbatch --dependency=afterok:${jobid_real[0]} -A $PROJECTID -p node -n 16 -t 240:00:00 -J "recal_"$NORMAL -e recal_normal.err -o recal_normal.out $SCRIPTSDIR"/"run_recal.sh $NORMAL".md.real.bam" $CONFIGFILE &)
recal_tumor=$(sbatch --dependency=afterok:${jobid_real[0]} -A $PROJECTID -p node -n 16 -t 240:00:00 -J "recal_"$TUMOR -e recal_tumor.err -o recal_tumor.out $SCRIPTSDIR"/"run_recal.sh $TUMOR".md.real.bam" $CONFIGFILE &)
IFS='Sumbitted batch job ' read -a jobid_recal_normal <<< $recal_normal
IFS='Sumbitted batch job ' read -a jobid_recal_tumor <<< $recal_tumor

#Freebayes - still needed to call germline variants for HeterozygoteConcordance.
#Freebayes for tumor not really used.
freebayes=$(sbatch --dependency=afterok:${jobid_recal_normal[0]}:${jobid_recal_tumor[0]} -A $PROJECTID -p node -n 16 -t 240:00:00 -J "freebayes_"$NORMAL"_"$TUMOR -e freebayes.err -o freebayes.out $SCRIPTSDIR"/"run_freebayes.sh $NORMAL".md.real.recal.bam" $TUMOR".md.real.recal.bam" $CONFIGFILE &)
IFS='Sumbitted batch job ' read -a jobid_freebayes <<< $freebayes

#HeterozygoteConcordance
hzc=$(sbatch --dependency=afterok:${jobid_freebayes[0]} -A $PROJECTID -p node -n 16 -t 240:00:00 -J "heterozygoteConcordance_"$NORMAL"_"$TUMOR -e heterozygoteConcordance.err -o heterozygoteConcordance.out $SCRIPTSDIR"/"run_heterozygoteConcordance.sh $NORMAL $TUMOR $CONFIGFILE &)
IFS='Sumbitted batch job ' read -a jobid_hzc <<< $hzc

#MuTect1 - needed because not sure how good the MuTect2 results are
mutect1=$(sbatch --dependency=afterok:${jobid_recal_normal[0]}:${jobid_recal_tumor[0]} -A $PROJECTID -p core -n 2 -t 240:00:00 -J "mutect1_"$NORMAL"_"$TUMOR -e mutect1.err -o mutect1.out $SCRIPTSDIR"/"run_mutect1.sh $NORMAL".md.real.recal.bam" $TUMOR".md.real.recal.bam" $CONFIGFILE &)
IFS='Sumbitted batch job ' read -a jobid_mutect1 <<< $mutect1

#Strelka
strelka=$(sbatch --dependency=afterok:${jobid_recal_normal[0]}:${jobid_recal_tumor[0]} -A $PROJECTID -p node -n 16 -t 240:00:00 -J "strelka_"$NORMAL"_"$TUMOR -e strelka.err -o strelka.out $SCRIPTSDIR"/"run_strelka.sh $NORMAL".md.real.recal.bam" $TUMOR".md.real.recal.bam" $CONFIGFILE &)
IFS='Sumbitted batch job ' read -a jobid_strelka <<< $strelka


#MuTect2
#Analyze each chr separately. All jobs dependent on the base quality score step above.
logdir="mutect2_log"
mkdir -p $logdir
joblist_mutect2=""
mergelist_mutect2=""
cleanlist_mutect2=""
for chr in `seq 1 22` X Y
do
mutect2=$(sbatch --dependency=afterok:${jobid_recal_normal[0]}:${jobid_recal_tumor[0]} -A $PROJECTID -p core -n 2 -t 240:00:00 -J "$SPARI_mutect2_$chr" -e "$logdir/mutect2_"$chr".err" -o "$logdir/mutect2_"$chr".out" $SCRIPTSDIR"/"run_mutect2_chr.sh $NORMAL".md.real.recal.bam" $TUMOR".md.real.recal.bam" $CONFIGFILE $chr &)
IFS='Sumbitted batch job ' read -a jobid_mutect2 <<< $mutect2
joblist_mutect2=$joblist_mutect2${jobid_mutect2[0]}:
mergelist_mutect2=$mergelist_mutect2" $SPAIR.mutect2.$chr.vcf.gz"
cleanlist_mutect2=$cleanlist_mutect2" $SPAIR.mutect2.$chr.vcf.gz $SPAIR.mutect2.$chr.vcf.gz.tbi"
done
joblist_mutect2=${joblist_mutect2%?}
merge=$(sbatch --dependency=afterok:$joblist_mutect2 -A $PROJECTID -p core -t 240:00:00 -J $SPAIR"_merge_chr_vcfs" -e "$logdir/merge_chr_vcfs.err" -o $SPAIR".mutect2.vcf" vcf-concat $mergelist_mutect2)
IFS='Sumbitted batch job ' read -a jobid_merge <<< $merge
clean=$(sbatch --dependency=afterok:${jobid_merge[0]} -A $PROJECTID -p core -t 240:00:00 -J $SPAIR"_clean" -e "$logdir/clean.err" -o "$logdir/clean.out"  $SCRIPTSDIR"/"clean_mutect2.sh $cleanlist_mutect2)
fi


