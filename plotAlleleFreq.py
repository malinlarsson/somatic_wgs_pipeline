#! /usr/bin/env python

import sys, re, math, random
import numpy as np
import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt
import matplotlib.patches as patches

from matplotlib.backends.backend_pdf import PdfPages

if len(sys.argv)<4:
    print "Usage: %s <vcffile> <outfile> <title> \n\n<vcffile> = Input vcf\n<outfile> = root name of output file\n<title> = title of plot, should reflect the vcf file\n\nCreates a pdf file with plots of alternative allele frequency (based on read counts) distributions for samples in a VCF file.\n\n" %sys.argv[0]
    sys.exit(0)


vcffile = sys.argv[1]
out = sys.argv[2]
main=sys.argv[3]

#Extract sample names from VCF file
sample_name=[]
for line in open(vcffile, 'r'):
    line=line.strip()
    if line.startswith("#CHROM"):
        info=line.split("\t")
        for col in range(9, len(info)):
            sample_name.append(info[col])
        break

#Create vector with chromosome names
chromosomes={"1":0, "2":1, "3":2, "4":3, "5":4, "6":5, "7":6, "8":7, "9":8, "10":9, "11":10, "12":11, "13":12, "14":13, "15":14, "16":15, "17":16, "18":17, "19":18, "20":19, "21":20, "22":21, "X":22, "Y":23}
for nr in chromosomes:
    print "chr "+nr+" has index "+str(chromosomes[nr])



#create empty arrays of allele freqs
AF={}
nsites={}

for s in sample_name:
    AFS=[]
    for c in chromosomes:
        print c
        AFS.append([])
    AF[s]=AFS
    nsites[s]=0
print "samplename"+sample_name[0]
print "samplename"+sample_name[1]

#print AF[sample_name[0]]


for snp in open(vcffile, 'r'):
    snp=snp.strip()
    if not snp.startswith("#"):
        snp_info=snp.split("\t")
        
        this_chr=snp_info[0]
        #print "this site belongs to chr "+this_chr
        
        # extract the position of the "FA" value
        format = snp_info[8].split(":")
        idx         = 0       
        for i in range(0, len(format)):
          if 'FA' in format[i]:
            idx = i
          else:
           continue

        for samplecol in range(9, len(snp_info)):
            
            if not ((snp_info[samplecol] == ".") or (snp_info[samplecol] == "0")):
                #this_site_freqs[col-9]=0
                #else:
                genotype_info=snp_info[samplecol].split(":")
                # for those cases were you have multiple variants at the same position.               
                if not "," in genotype_info[idx]:
                    #print "af for "+sample_name[samplecol-9]+": "+genotype_info[idx]
                    if this_chr in chromosomes:
                        AF[sample_name[samplecol-9]][chromosomes[this_chr]].append(float(genotype_info[idx]))
                        nsites[sample_name[samplecol-9]]=nsites[sample_name[samplecol-9]]+1




for c in chromosomes:
    print "chromosome "+c
    for s in sample_name:
        print "N sites for "+s+" on chr"+c+" :"+str(len(AF[s][chromosomes[c]]))
        print "mean AF for "+s+" on chr"+c+" :"+str(np.mean(AF[s][chromosomes[c]]))
for s in sample_name:
    print "Total sites for "+s+": "+str(nsites[s])


#simple boxplot
fig, ax = plt.subplots(figsize=(10, 6))

plt.subplots_adjust(left=0.075, right=0.95, top=0.9, bottom=0.25)

bp1 =plt.boxplot(AF[sample_name[0]], notch=0, sym='+', vert=1)
plt.setp(bp1['boxes'], color='tomato')
plt.setp(bp1['whiskers'], color='tomato')
plt.setp(bp1['caps'], color='tomato')
plt.setp(bp1['fliers'], color='tomato', marker='+')
plt.setp(bp1['medians'], color='red', linewidth=1.5)

bp2 = plt.boxplot(AF[sample_name[1]], notch=0, sym='+', vert=1)
plt.setp(bp2['boxes'], color='darkseagreen')
plt.setp(bp2['whiskers'], color='darkseagreen')
plt.setp(bp2['caps'], color='darkseagreen')
plt.setp(bp2['fliers'], color='darkseagreen', marker='+')
plt.setp(bp2['medians'], color='darkgreen', linewidth=1.5)


# Add a horizontal grid to the plot, but make it very light in color
# so we can use it for reading data values but not be distracting
ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey',
               alpha=0.5)

# Hide these grid behind plot objects
ax.set_axisbelow(True)
ax.set_title(main)
ax.set_ylabel('Alt. allele frequency based on read counts', fontsize=10)
ax.set_xlabel('chromosome', fontsize=10)

# Set the X-axis ranges and axes labels
xticks=['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', 'X', 'Y']
x=range(1, 25)
plt.xticks(x, xticks, fontsize=10)

# Finally, add a basic legend
# axes coordinates are 0,0 is bottom left and 1,1 is upper right
p = patches.Rectangle(
                     (21.5, 0.82), 2.2 , 0.14 , fill=False)
ax.add_patch(p)
plt.figtext(0.85, 0.84, sample_name[0], backgroundcolor='white', color='red', size='small')
plt.figtext(0.85, 0.80, sample_name[1], backgroundcolor='white', color='darkgreen', size='small')


# Save the figure
fig.savefig(out+'.png', bbox_inches='tight')



##Create plots and print to PDF file
#pp = PdfPages(out+'.pdf')
##Boxplot
#x=range(1, len(chromosomes)+1)
#plt.boxplot(sample2)
#plt.xticks(x, sample_name, rotation=45)
#plt.title('Alt allele frequency distributions in '+vcffile+'\n')
#plt.ylabel('Alt allele frequency based on read depths')
#plt.margins(0.1)
#plt.subplots_adjust(bottom=0.3)
#plt.savefig(pp, format='pdf')
#plt.clf()
#
##Histogram
#n, bins,patches=plt.hist(allele_freq, label=sample_name)
#plt.legend()
#plt.xlabel('Alt allele freq based on read depths')
#plt.ylabel('Counts (SNVs)')
#plt.title('Alt allele frequency distributions in '+vcffile+'\n')
#plt.savefig(pp, format='pdf')
##plt.show()
#pp.close()
#
#print 'printed results to '+out+'.pdf'

