#! /usr/bin/env python

import sys, re


if len(sys.argv)<3:
    print "Usage: %s <allelecounts> <loci_subset> <outfile>\n" %sys.argv[0]
    sys.exit(0)


allelecounts = sys.argv[1]
locifile = sys.argv[2]
outfile = sys.argv[3]

of = open(outfile, 'w')

SNP_pos= {}
lf = open(locifile, 'r')
for l in lf:
    l=l.strip()
    linfo=l.split()
    pos=linfo[0]+'_'+linfo[1]
    #print pos
    SNP_pos[pos]=1

acf = open(allelecounts, 'r')
for ac in acf:
    ac=ac.strip()
    acinfo=ac.split()
    pos=acinfo[0]+'_'+acinfo[1]
    #print pos
    if pos.startswith('#'):
        of.write("%s\n" %ac)
    elif pos in SNP_pos:
        #print pos
            of.write("%s\n" %ac)

of.close()