#! /bin/bash

USAGE="\n

Usage: <sbatch syntax> $0 normal.bam tumor.bam configfile.sh \n\n

Where\n 
<sbatch syntax> is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p node -n 16  -t 24:00:00 -J bam2vcf
-e somename.err -o somename.out\n
and\n
configfile.sh contains paths to reference files and project info etc.  \n\n"

if test -z $3; then
	echo -e $USAGE
	exit
fi

#Input data
NORMAL=$1
TUMOR=$2
CONFIGFILE=$3


#Modules
module load bioinfo-tools
module load samtools
module load VarScan/2.3.7


#Reading config file
. $CONFIGFILE

echo "normal bam file: "$NORMAL
echo "tumor bam file: "$TUMOR

samtools mpileup -q 1 -f $REFERENCE $NORMAL $TUMOR | java -jar $VARSCAN_HOME/VarScan.jar copynumber varScan --mpileup 1

java -jar $VARSCAN_HOME/VarScan.jar copyCaller varScan.copynumber --output-file varScan.copynumber.called
