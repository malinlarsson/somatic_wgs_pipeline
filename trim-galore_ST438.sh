#!/bin/bash -l
#SBATCH -A b2011185
#SBATCH -p core -n 1
#SBATCH -J trim-galore_ST438
#SBATCH -t 240:00:00
#SBATCH -o trimgalore_ST438.output
#SBATCH -e trimgalore_ST438.error
sg b2011185
module add bioinfo-tools TrimGalore/0.4.1
trim_galore --paired /proj/b2011185/nobackup/private/wabi/data/ST438N/5_150310_H2TCWCCXX-11_ADM965A1-dual57_TAATGCGC_1.fastq.gz /proj/b2011185/nobackup/private/wabi/data/ST438N/5_150310_H2TCWCCXX-11_ADM965A1-dual57_TAATGCGC_2.fastq.gz
trim_galore --paired /proj/b2011185/nobackup/private/wabi/data/ST438N/5_150310_H2TCWCCXX-21_ADM965A1-dual57_TAATGCGC_1.fastq.gz /proj/b2011185/nobackup/private/wabi/data/ST438N/5_150310_H2TCWCCXX-21_ADM965A1-dual57_TAATGCGC_2.fastq.gz
trim_galore --paired /proj/b2011185/nobackup/private/wabi/data/ST438T/6_150310_H2TCWCCXX-11_ADM965A2-dual58_TAATGCGC_1.fastq.gz /proj/b2011185/nobackup/private/wabi/data/ST438T/6_150310_H2TCWCCXX-11_ADM965A2-dual58_TAATGCGC_2.fastq.gz
trim_galore --paired /proj/b2011185/nobackup/private/wabi/data/ST438T/6_150310_H2TCWCCXX-21_ADM965A2-dual58_TAATGCGC_1.fastq.gz /proj/b2011185/nobackup/private/wabi/data/ST438T/6_150310_H2TCWCCXX-21_ADM965A2-dual58_TAATGCGC_2.fastq.gz
trim_galore --paired /proj/b2011185/nobackup/private/wabi/data/ST438T/7_150310_H2TCWCCXX-11_ADM965A2-dual58_TAATGCGC_1.fastq.gz /proj/b2011185/nobackup/private/wabi/data/ST438T/7_150310_H2TCWCCXX-11_ADM965A2-dual58_TAATGCGC_2.fastq.gz
trim_galore --paired /proj/b2011185/nobackup/private/wabi/data/ST438T/7_150310_H2TCWCCXX-21_ADM965A2-dual58_TAATGCGC_1.fastq.gz /proj/b2011185/nobackup/private/wabi/data/ST438T/7_150310_H2TCWCCXX-21_ADM965A2-dual58_TAATGCGC_2.fastq.gz
echo finished!
