#! /bin/bash

USAGE="\n

Usage: <sbatch syntax> $0 normal.bam tumor.bam configfile.sh chr\n\n

Where\n 
<sbatch syntax> is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p node -n 16  -t 24:00:00 -J bam2vcf
-e somename.err -o somename.out\n
configfile.sh contains paths to reference files and project info etc.
chr is the current chromosome number.\n\n"

if test -z $4; then
	echo -e $USAGE
	exit
fi

#Input data
NORMAL=$1
TUMOR=$2
CONFIGFILE=$3
CHR=$4

#Modules
module load bioinfo-tools
module load GATK
module load vcftools
module load tabix
set -euo pipefail

#Reading config file
. $CONFIGFILE

echo "normal bam file: $NORMAL"
echo "tumor bam file: $TUMOR"

normalprefix=${NORMAL%".md.real.recal.bam"}
tumorprefix=${TUMOR%".md.real.recal.bam"}
SPAIR=$normalprefix"_"$tumorprefix
echo "Sample pair "$SPAIR

fail=1
while [ $fail != 0 ]; do
echo "attempt mutect2 on chr $CHR"
java -jar $GATK_HOME/GenomeAnalysisTK.jar \
-T MuTect2 \
-R $REFERENCE \
--cosmic $COSMIC \
--dbsnp $DBSNP \
-I:normal $NORMAL \
-I:tumor $TUMOR \
-o $SPAIR".mutect2.$CHR.vcf" \
-L $CHR
fail=$?
echo "exit status: $fail"
done
if test $fail = 0;then
echo "MuTect2 on chr $CHR went well!"

#Remove previous versions of the bgzipped and tabixed file if present
if [ -f "$SPAIR.mutect2.$CHR.vcf.gz" ];then
rm $SPAIR".mutect2.$CHR.vcf.gz"
fi
if [ -f "$SPAIR.mutect2.$CHR.vcf.gz.tbi" ];then
rm $SPAIR".mutect2.$CHR.vcf.gz.tbi"
fi
if [ -f "$SPAIR.mutect2.$CHR.vcf.idx" ];then
rm $SPAIR".mutect2.$CHR.vcf.idx"
fi
#bgzip and tabix
bgzip $SPAIR".mutect2.$CHR.vcf"
tabix $SPAIR".mutect2.$CHR.vcf.gz"
fi



