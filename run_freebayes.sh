#! /bin/bash

USAGE="\n

Usage: <sbatch syntax> $0 normal.bam tumor.bam configfile.sh \n\n

Where\n
<sbatch syntax> is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p node -n 16  -t 24:00:00 -J bam2vcf
-e somename.err -o somename.out\n
and\n
configfile.sh contains paths to reference files and project info etc.  \n\n"

if test -z $3; then
echo -e $USAGE
exit
fi

#Input data
NORMAL=$1
TUMOR=$2
CONFIGFILE=$3


#Modules
module load bioinfo-tools
module load freebayes
module load vcftools
set -euo pipefail

#Reading config file
. $CONFIGFILE

echo "normal bam file: "$NORMAL
echo "tumor bam file: "$TUMOR

normalprefix=${NORMAL%".md.real.recal.bam"}
tumorprefix=${TUMOR%".md.real.recal.bam"}

SPAIR=$normalprefix"_"$tumorprefix
echo "Sample pair "$SPAIR

freebayes -f $REFERENCE $NORMAL > $normalprefix".freebayes.vcf"
if test $? = 0;then
    echo "Freebayes for $NORMAL went well!"
fi


freebayes -f $REFERENCE --pooled-continuous --pooled-discrete -F 0.03 -C 2 $TUMOR $NORMAL > $SPAIR".freebayes.vcf"
if test $? = 0;then
    echo "Freebayes for $TUMOR vs $NORMAL went well!"
fi

