#! /bin/bash

USAGE="\n

Usage: <sbatch syntax> $0 normal.bam tumor.bam\n\n

Where\n
<sbatch syntax> is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p core -n 1  -t 24:00:00 -J jobname
-e somename.err -o somename.out\n
\n\n"

if test -z $2; then
echo -e $USAGE
exit
fi

#Input data
NORMALBAM=$1
TUMORBAM=$2
NORMAL=${NORMALBAM%".md.real.recal.bam"}
TUMOR=${TUMORBAM%".md.real.recal.bam"}
set -euo pipefail

/home/malin/glob/snp-pileup/snp-pileup.pl $NORMALBAM $TUMORBAM $NORMAL"_"$TUMOR.snp-pileup

if test $? = 0;then
echo "snp-pileup test went well!"
fi

#Then start the FACETS R script