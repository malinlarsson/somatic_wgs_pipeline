#! /bin/bash

USAGE="\n

Usage: <sbatch syntax> $0 bam\n\n

Where\n
<sbatch syntax> is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p node -n 16  -t 24:00:00 -J bam2vcf
-e somename.err -o somename.out\n
bam = input bam \n\n"

if test -z $1; then
echo -e $USAGE
exit
fi

#Modules
module load bioinfo-tools
module load picard
set -euo pipefail
#Input
BAM=$1

prefix=${BAM%".bam"}


###############################################
#
# Mark duplicates
#
###############################################

echo "#######  Duplicate marking (picard)"
echo "Infile = "$BAM
echo "Outfile = "$prefix".md.bam"

java -jar $PICARD_HOME"/MarkDuplicates.jar" \
INPUT=$BAM \
OUTPUT=$prefix".md.bam" \
METRICS_FILE=$prefix".md_metrics" \
CREATE_INDEX=true \
VALIDATION_STRINGENCY=LENIENT \
REMOVE_DUPLICATES=false

if test $? = 0;then
echo "Duplicate marking of $BAM completed"
rm $BAM
    if [ -f $prefix".bai" ]; then
        rm $prefix".bai"
    elif [ -f $prefix".bam.bai" ]; then
        rm $prefix".bam.bai"
    fi
fi
