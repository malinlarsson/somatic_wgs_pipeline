#! /bin/bash


USAGE="\ncalls somatic mutations in a tumor/normal sample pair.\n\n
Usage: $0 normal tumor configfile\n
Where\n
normal and tumor are sample ids.\n"

if test -z $3; then
echo -e $USAGE
exit
fi

module load bioinfo-tools
module load samtools
module load picard
module load bwa
module load tabix
module load vcftools


#Input to script
NORMAL=$1
TUMOR=$2
CONFIGFILE=$3

#Import config file:
. $CONFIGFILE

#path to results and temp folders
SPAIR=$NORMAL'_'$TUMOR


#MuTect2
#Analyze each chr separately
$SCRIPTSDIR"/"run_mutect2.sh $NORMAL".md.real.recal.bam" $TUMOR".md.real.recal.bam" $CONFIGFILE &

