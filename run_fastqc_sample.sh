#! /bin/bash

USAGE="\n\nUsage:$0 configfile\n\n
Where\n
sbatch syntax is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p core -n 1 -t 24:00:00 -J jobname
-e xxx.err -o xxx.out\n
\n
Start from analysis directory of a sample  \n\n"

if test -z $1; then
echo -e $USAGE
exit
fi

#Input to script
CONFIGFILE=$1
. $CONFIGFILE

module load bioinfo-tools
module load FastQC
set -euo pipefail

for fastq in *$FASTQ_SUFFIX
do
    echo "current fastq file: $fastq"
    sbatch -A $PROJECTID -p core -n 1 -t 24:00:00 -J "fastqc_"$fastq -e "fastqc_"$fastq".err" -o "fastqc_"$fastq".out"  $SCRIPTSDIR"/"run_fastqc.sh $fastq &
done
