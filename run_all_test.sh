#! /bin/bash


USAGE="\ncalls somatic mutations in a tumor/normal sample pair.\n\n
Usage: $0 normal tumor configfile\n
Where\n
normal and tumor are sample ids.\n"

if test -z $3; then
echo -e $USAGE
exit
fi

module load bioinfo-tools
module load samtools
module load picard
module load bwa
module load tabix

#Call to script:
#run_all.sh $NORMAL $TUMOR $CONFIGFILE &

#Input to script
NORMAL=$1
TUMOR=$2
CONFIGFILE=$3

#Import config file:
. $CONFIGFILE

#path to results and temp folders
SPAIR=$NORMAL'_'$TUMOR

#TEMPDIR=$SNIC_TMP"/"$SAMPLE
RESDIR=$PWD"/"$SPAIR
#TEMPDIR=$RESDIR"/tmp"

echo "RESDIR: "$RESDIR
#echo "TEMPDIR: "$TEMPDIR
echo "DATADIR: "$DATADIR


mkdir -p $RESDIR
#mkdir -p $TEMPDIR

cd $RESDIR
if test $? == 0;then

#Run bwa
echo "Calling run_bwa.sh for "$NORMAL
bwa_normal=$(sbatch -A $PROJECTID -p core -n 4 -t 15:00 --qos=short -J "bwa_"$NORMAL -e "bwa_"$NORMAL".err" -o "bwa_"$NORMAL".out"  $SCRIPTSDIR"/"run_bwa.sh $NORMAL $CONFIGFILE &)
echo "Calling run_bwa.sh for "$TUMOR
bwa_tumor=$(sbatch -A $PROJECTID -p core -n 4 -t 15:00 --qos=short -J "bwa_"$TUMOR -e "bwa_"$TUMOR".err" -o "bwa_"$TUMOR".out"  $SCRIPTSDIR"/"run_bwa.sh $TUMOR $CONFIGFILE &)
echo "idtext: "$bwa_normal
IFS='Sumbitted batch job ' read -a jobid_bwa_normal <<< $bwa_normal
IFS='Sumbitted batch job ' read -a jobid_bwa_tumor <<< $bwa_tumor

#Merge primary bam files
merge_normal=$(sbatch --dependency=afterok:${jobid_bwa_normal[0]} -A $PROJECTID -p core -n 4 -t 15:00 --qos=short-J "merge_"$NORMAL -e merge_normal.err -o merge_normal.out $SCRIPTSDIR"/"merge_primary_bams.sh $NORMAL &)
merge_tumor=$(sbatch --dependency=afterok:${jobid_bwa_tumor[0]} -A $PROJECTID -p core -n 4 -t 15:00 --qos=short -J "merge_"$TUMOR -e merge_tumor.err -o merge_tumor.out $SCRIPTSDIR"/"merge_primary_bams.sh $TUMOR &)

IFS='Sumbitted batch job ' read -a jobid_merge_normal <<< $merge_normal
IFS='Sumbitted batch job ' read -a jobid_merge_tumor <<< $merge_tumor

#mark duplicates
md_normal=$(sbatch --dependency=afterok:${jobid_merge_normal[0]} -A $PROJECTID --p core -n 4 -t 15:00 --qos=short -J "md_"$NORMAL -e md_normal.err -o md_normal.out $SCRIPTSDIR"/"run_md.sh $NORMAL".bam" &)
md_tumor=$(sbatch --dependency=afterok:${jobid_merge_tumor[0]} -A $PROJECTID -p core -n 4 -t 15:00 --qos=short -J "md_"$TUMOR -e md_tumor.err -o md_tumor.out $SCRIPTSDIR"/"run_md.sh $TUMOR".bam" &)
IFS='Sumbitted batch job ' read -a jobid_md_normal <<< $md_normal
IFS='Sumbitted batch job ' read -a jobid_md_tumor <<< $md_tumor

#indel realignment
real=$(sbatch --dependency=afterok:${jobid_md_normal[0]}:${jobid_md_tumor[0]} -A $PROJECTID -p core -n 4 -t 15:00 --qos=short -J "real_"$NORMAL"_"$TUMOR -e real.err -o real.out $SCRIPTSDIR"/"run_joint_real.sh $NORMAL".md.bam" $TUMOR".md.bam" $CONFIGFILE &)
IFS='Sumbitted batch job ' read -a jobid_real <<< $real

#base quality score recalibration
recal_normal=$(sbatch --dependency=afterok:${jobid_real[0]} -A $PROJECTID -p core -n 4 -t 15:00 --qos=short -J "recal_"$NORMAL -e recal_normal.err -o recal_normal.out $SCRIPTSDIR"/"run_recal.sh $NORMAL".md.real.bam" $CONFIGFILE &)
recal_tumor=$(sbatch --dependency=afterok:${jobid_real[0]} -A $PROJECTID -p core -n 4 -t 15:00 --qos=short -J "recal_"$TUMOR -e recal_tumor.err -o recal_tumor.out $SCRIPTSDIR"/"run_recal.sh $TUMOR".md.real.bam" $CONFIGFILE &)
IFS='Sumbitted batch job ' read -a jobid_recal_normal <<< $recal_normal
IFS='Sumbitted batch job ' read -a jobid_recal_tumor <<< $recal_tumor

#Freebayes
freebayes=$(sbatch --dependency=afterok:${jobid_recal_normal[0]}:${jobid_recal_tumor[0]} -A $PROJECTID -p core -n 4 -t 15:00 --qos=short -J "freebayes_"$NORMAL"_"$TUMOR -e freebayes.err -o freebayes.out $SCRIPTSDIR"/"run_freebayes.sh $NORMAL".md.real.recal.bam" $TUMOR".md.real.recal.bam" $CONFIGFILE &)
IFS='Sumbitted batch job ' read -a jobid_freebayes <<< $freebayes

#HeterozygoteConcordance
hzc=$(sbatch --dependency=afterok:${jobid_freebayes[0]} -A $PROJECTID -p core -n 4 -t 15:00 --qos=short -J "heterozygoteConcordance_"$NORMAL"_"$TUMOR -e heterozygoteConcordance.err -o heterozygoteConcordance.out $SCRIPTSDIR"/"run_heterozygoteConcordance.sh $NORMAL $TUMOR $CONFIGFILE &)
IFS='Sumbitted batch job ' read -a jobid_hzc <<< $hzc

#MuTect2
mutect2=$(sbatch --dependency=afterok:${jobid_recal_normal[0]}:${jobid_recal_tumor[0]} -A $PROJECTID -p core -n 4 -t 15:00 --qos=short -J "mutect2_"$NORMAL"_"$TUMOR -e mutect2.err -o mutect2.out $SCRIPTSDIR"/"run_mutect2.sh $NORMAL".md.real.recal.bam" $TUMOR".md.real.recal.bam" $CONFIGFILE &)
IFS='Sumbitted batch job ' read -a jobid_mutect2 <<< $mutect2


fi

