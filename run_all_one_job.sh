#! /bin/bash

USAGE="\n
Usage: sbatch syntax $0 normal tumor configfile\n
Where\n
sbatch syntax is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p node -n 16  -t 24:00:00 -J bam2vcf
-e somename.err -o somename.out\n
normal and tumor are sample ids.\n"

if test -z $3; then
echo -e $USAGE
exit
fi

module load bioinfo-tools
module load samtools
module load picard
module load bwa
module load vcftools
module load tabix

#Call to script:
#run_all.sh $NORMAL $TUMOR $CONFIGFILE &

#Input to script
NORMAL=$1
TUMOR=$2
CONFIGFILE=$3

#Import config file:
. $CONFIGFILE

#path to results and temp folders
SPAIR=$NORMAL'_'$TUMOR

#TEMPDIR=$SNIC_TMP"/"$SAMPLE
RESDIR=$PWD"/"$SPAIR
#TEMPDIR=$SNIC_TMP"/"$SPAIR
TEMPDIR=$PWD
echo "RESDIR: "$RESDIR
echo "TEMPDIR: "$TEMPDIR
echo "DATADIR: "$DATADIR


mkdir -p $RESDIR
mkdir -p $TEMPDIR

cd $TEMPDIR
if test $? == 0;then

#Run bwa
echo "Calling run_bwa.sh for "$NORMAL
$SCRIPTSDIR"/"run_bwa.sh $NORMAL $CONFIGFILE &
echo "Calling run_bwa.sh for "$TUMOR
$SCRIPTSDIR"/"run_bwa.sh $TUMOR $CONFIGFILE &
wait %1 %2

#Merge primary bam files
$SCRIPTSDIR"/"merge_primary_bams.sh $NORMAL &
$SCRIPTSDIR"/"merge_primary_bams.sh $TUMOR &
wait %1 %2

#mark duplicates
$SCRIPTSDIR"/"run_md.sh $NORMAL".bam" &
$SCRIPTSDIR"/"run_md.sh $TUMOR".bam" &
wait %1 %2
cp $NORMAL".md_metrics" $RESDIR/.
cp $TUMOR".md_metrics" $RESDIR/.

#indel realignment
$SCRIPTSDIR"/"run_joint_real.sh $NORMAL".md.bam" $TUMOR".md.bam" $CONFIGFILE &
wait
cp $NORMAL".md_"$TUMOR".md.intervals" $RESDIR/.

#base quality score recalibration
$SCRIPTSDIR"/"run_recal.sh $NORMAL".md.real.bam" $CONFIGFILE &
$SCRIPTSDIR"/"run_recal.sh $TUMOR".md.real.bam" $CONFIGFILE &
wait %1 %2
cp $NORMAL".md.real.table" $RESDIR/.
cp $NORMAL".md.real.recal.table" $RESDIR/.
cp $NORMAL".md.real.recal_plots.pdf" $RESDIR/.
cp $TUMOR".md.real.table" $RESDIR/.
cp $TUMOR".md.real.recal.table" $RESDIR/.
cp $TUMOR".md.real.recal_plots.pdf" $RESDIR/.
cp $NORMAL".md.real.recal.bam" $RESDIR"/".
cp $NORMAL".md.real.recal.bai" $RESDIR"/".
cp $TUMOR".md.real.recal.bam" $RESDIR"/".
cp $TUMOR".md.real.recal.bai" $RESDIR"/".


#Freebayes
$SCRIPTSDIR"/"run_freebayes.sh $NORMAL".md.real.recal.bam" $TUMOR".md.real.recal.bam" $CONFIGFILE &
wait
cp $NORMAL".freebayes.vcf" $RESDIR"/."
cp $SPAIR".freebayes.vcf" $RESDIR"/."

#HeterozygoteConcordance
$SCRIPTSDIR/run_heterozygoteConcordance.sh $NORMAL $TUMOR $CONFIGFILE &
wait
cp $SPAIR".heterozygoteConcordance" $RESDIR"/."


#MuTect2

mergelist_mutect2=""
for chr in `seq 1 22` X Y
do
$SCRIPTSDIR"/"run_mutect2_chr.sh $NORMAL".md.real.recal.bam" $TUMOR".md.real.recal.bam" $CONFIGFILE $chr
mergelist_mutect2=$mergelist_mutect2" $SPAIR.mutect2.$chr.vcf.gz"
done
vcf-concat $mergelist_mutect2 > $SPAIR.mutect2.vcf
cp $SPAIR".mutect2.vcf" $RESDIR"/."


fi

