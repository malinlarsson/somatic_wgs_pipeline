#! /bin/bash

USAGE="\n\nUsage: sbatch syntax $0 fastq_1 fastq_2\n\n
Where\n
sbatch syntax is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p core -n 1 -t 72:00:00 -J jobname
-e xxx.err -o xxx.out\n
and\n
fastq_1 and fastq_2 contain paired-end reads.\n\n"

if test -z $2; then
echo -e $USAGE
exit
fi

module load bioinfo-tools
module load TrimGalore/0.4.1

echo "Starting TrimGalore analysis of file $1 and $2"

trim_galore --paired $1 $2


