#! /bin/bash


USAGE="\nMerge results from mutect run on individual chromosomes. \n

Usage: <sbatch syntax> $0 $filelist \n\n

Where\n
<sbatch syntax> is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p node -n 16  -t 24:00:00 -J mutect
-e somename.err -o somename.out\n
filelist is a list of all files that shoud be removed \n\n"


if test -z $1; then
echo -e $USAGE
exit
fi

#Looping over all chromosomes in input array (i.e. in $1, $2 ect to $#)
for file in ${@:1}
do
    if [ -f "$file" ];then
        rm $file
    fi
done
