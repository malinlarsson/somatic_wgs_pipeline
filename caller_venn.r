library(venneuler)
setwd("~/wabi/caw/compare_callers")


#ST438
outfile="ST438.callervenn.jpeg"
jpeg(file = outfile, width = 1200, height = 1200, pointsize = 30, bg = "white")
MyVenn <- venneuler(c(A=4115,B=1658,C=1628,"A&B"=610,
"A&C"=568,"B&C"=479,"A&B&C"=455))
MyVenn$labels <- c("MuTect1\n4115","MuTect2\n1658","Strelka\n1628")
plot(MyVenn)
#Mutect1&Mutect2
text(0.47,0.4,"610")
#Mutect1&Strelka
text(0.47, 0.65, "568")
#Mutect2&Strelka
text(0.63,0.52,"479")
#Mutect1&Mutect2&Strelka
text(0.51,0.51,"455")
dev.off()